#[macro_use] 
extern crate log; 
extern crate log4rs;
use walkdir::WalkDir;
use logging::get_logging_config;

use std::{env, io, io::prelude::*, fs, process::exit, process::Command};

use parser::get_permissions_row_for_path;

use crate::parser::FilePermissions;
mod logging;
mod parser;

/// Show error and quit
fn show_unsupported_cmd_error() {
    println!("error > unsupported command");
    exit(1);
}

fn print_permissions_for_path(path: &str) {
    for entry in WalkDir::new(path).follow_links(true)
            .into_iter()
            .filter_map(Result::ok) {
        let file_path = String::from(entry.path().to_string_lossy());
        match get_permissions_row_for_path(&file_path) {
            Ok(perms) => println!("{}", perms),
            Err(e) => eprintln!("{}", e.root_cause())
        }
    }
    
}

fn main() {
    let logging_config = get_logging_config("debug");     
    log4rs::init_config(logging_config).unwrap();

    let args: Vec<String> = env::args().collect();
    
    if args.len() == 3 {
        let cmd: &str = args.get(1).expect("unable to get argument");

        if cmd == "scan" {
            let path: &str = args.get(2).expect("unable to get path argument value");
            print_permissions_for_path(path);

        } else {
            show_unsupported_cmd_error();
        }

    } else if args.len() == 2 {
        let cmd: &str = args.get(1).expect("unable to get argument");

        if cmd == "recovery" {
            println!("recovery permissions..");
            let stdin = io::stdin();

            for line in stdin.lock().lines() {
                let s = line.expect("unable to read from stdin");
                println!("- perms: '{}'", s);
                match FilePermissions::parse(&s) {
                    Ok(perms) => {
                        if FilePermissions::is_valid(&perms) {

                            match fs::metadata(&perms.file_path) {
                                Ok(metadata) => {

                                    let uid_gid_arg = format!("{}:{}", &perms.uid, &perms.gid);

                                    println!("mode: {}", &perms.mode);

                                    if metadata.is_symlink() {
                                        match Command::new("/usr/bin/chown").args(["-h", &uid_gid_arg, &perms.file_path]).output() {
                                            Ok(_) => println!("+ '{}'", &perms.file_path),
                                            Err(e) => eprintln!("chown error for '{}': {}", &perms.file_path, e)
                                        }
                                        match Command::new("/usr/bin/chmod").args(["-h", &perms.mode, &perms.file_path]).output() {
                                            Ok(_) => println!("+ '{}'", &perms.file_path),
                                            Err(e) => eprintln!("chmod error for '{}': {}", &perms.file_path, e)
                                        }   

                                    } else {
                                        match Command::new("/usr/bin/chown").args([&uid_gid_arg, &perms.file_path]).output() {
                                            Ok(_) => println!("+ '{}'", &perms.file_path),
                                            Err(e) => eprintln!("chown error for '{}': {}", &perms.file_path, e)
                                        }
                                        match Command::new("/usr/bin/chmod").args([&perms.mode, &perms.file_path]).output() {
                                            Ok(_) => println!("+ '{}'", &perms.file_path),
                                            Err(e) => eprintln!("chmod error for '{}': {}", &perms.file_path, e)
                                        }
                                    }
                                
                                }
                                Err(e) => eprintln!("error: unable to read metadata for '{}': {}", perms.file_path, e)
                            }

                        } else {
                            eprintln!("invalid permissions '{}'", &perms.mode);
                        }
                    },
                    Err(e) => eprintln!("error: {}", e)
                }

            }
        }

    } else {
        println!("============================================");
        println!(" FILE PERMISSION RECOVERY TOOL v0.1.0 alpha");
        println!("--------------------------------------------");
        println!("./permrec scan <path> - get permissions for given path (owner, group, rwx) in permrec format\n");
        println!("./permrec recovery - restore permissions based on given data");
    }
}
