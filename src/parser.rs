use std::os::unix::fs::PermissionsExt;
extern crate regex;
use std::os::unix::fs::MetadataExt;
use anyhow::{anyhow, Result};
use std::{fs, fmt};
use regex::Regex;
pub struct FilePermissions {
    pub file_path: String,
    pub uid: u32,
    pub gid: u32,
    pub mode: String
}

impl FilePermissions {
    pub fn parse(input: &str) -> Result<FilePermissions> {
        debug!("parse input '{}'", input);
        let parts = input.split("|").into_iter().collect::<Vec<&str>>();

        if parts.len() == 4 {
            let file_path: &str = parts.get(0).unwrap();
            let uid_str: &str = parts.get(1).unwrap();
            let gid_str: &str = parts.get(2).unwrap();
            let mode_str: &str = parts.get(3).unwrap();

            debug!("file_path: '{}'", file_path);
            debug!("uid: '{}'", uid_str);
            debug!("gid: '{}'", gid_str);
            debug!("mode: '{}'", mode_str);
            Ok(
                FilePermissions {
                    file_path: file_path.to_string(),
                    uid: FilePermissions::get_u32_from_str_or_zero(uid_str),
                    gid: FilePermissions::get_u32_from_str_or_zero(gid_str),
                    mode: mode_str.to_string()
                }
                
            )

        } else {
            Err(anyhow!("unsupported data format"))
        }
        
    }

    pub fn is_valid(perms: &FilePermissions) -> bool {
        let mut valid = true;

        if perms.file_path.len() == 0 {
            valid = false;
        }

        if perms.mode.len() == 0 {
            valid = false;
        }

        debug!("row valid: {}", valid);

        valid
    }

    fn get_u32_from_str_or_zero(input: &str) -> u32 {
        input.parse::<u32>().unwrap_or(0)
    }
}

impl fmt::Display for FilePermissions {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}|{}|{}|{}", self.file_path, self.uid, self.gid, self.mode)
    }
}

pub fn get_permissions_row_for_path(path: &str) -> Result<String> {
    debug!("get permissions row from path '{}'", path);
    match fs::metadata(&path) {
        Ok(metadata) => {
            let uid = metadata.uid();
            let gid = metadata.gid();
            let perms = metadata.permissions();
            let mode = perms.mode();

            let mode_str = format!("{:o}", mode);
            debug!("mode: '{}'", mode_str);

            let pattern = Regex::new("\\d+(\\d{3})$").unwrap();
            match pattern.captures(&mode_str) {
                Some(file_mode) => {
                    let group = file_mode.get(1).unwrap();
                    let row = format!("{}|{}|{}|{}", &path, uid, gid, group.as_str());
                    Ok(row)
                }
                None => {
                    Err(anyhow!("unsupported row format"))
                }
            }
        },
        Err(e) => { 
            eprintln!("metadata read error for '{}': {}", &path, e); 
            Err(anyhow!("metadata read error"))
        }
    }
}

#[cfg(test)]
mod tests {

    use crate::parser::FilePermissions;

    use super::get_permissions_row_for_path;

    #[test]
    fn valid_perms_expected() {
        let perms_rows = get_permissions_row_for_path("/etc/hosts").unwrap();

        assert_eq!("/etc/hosts|0|0|644", perms_rows);
    }

    #[test]
    fn return_false_for_invalid_file_path() {
        let perms = FilePermissions {
            file_path: String::new(),
            uid: 231, gid: 420, mode: "333".to_string()
        };
        assert!(!FilePermissions::is_valid(&perms));
    }

    #[test]
    fn return_false_for_blank_mode() {
        let perms = FilePermissions {
            file_path: String::from("/etc"),
            uid: 0, gid: 42124, mode: "".to_string()
        };
        assert!(!FilePermissions::is_valid(&perms));
    }

    #[test]
    fn return_true_for_valid_perms() {
        let perms = FilePermissions {
            file_path: String::from("/etc"),
            uid: 0, gid: 42124, mode: "1000".to_string()
        };
        assert!(FilePermissions::is_valid(&perms));
    }

    #[test]
    fn return_perms_from_valid_line() {
        let perms = FilePermissions::parse("/etc/systemd/some-file|0|1002|750").unwrap();
        assert_eq!(perms.file_path, "/etc/systemd/some-file");
        assert_eq!(perms.uid, 0);
        assert_eq!(perms.gid, 1002);
        assert_eq!(perms.mode, "750");
    }

    #[test]
    fn return_error_for_invalid_line_value() {
        assert!(FilePermissions::parse("").is_err());
        assert!(FilePermissions::parse("34j9gj4gjdsfgj43ijg4j").is_err());
    }

}
