# permrec

File permission recovery tool for Linux.

Helps in case when you accidentical made chmod / chown on root file system or any system files.

## Usage

1. Get correct data on healthly system

```shell
chmod +x permrec
./permrec scan > valid-perms.txt
```

2. Copy `valid-perms.txt` to target system (i.e. archiso + chroot + flash drive) and run:

```shell
./permrec recovery < valid-perms.txt
```

## Format

```csv
absolute-path|owner|group|permissions
```

### Examples:

```csv
/etc/vimrc|root|root|744
/etc/systemd|root|root|755
``